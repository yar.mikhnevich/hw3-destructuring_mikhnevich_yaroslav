  // Завдання 3
  // У нас є об'єкт' user:
  
  const user1 = {
    name: "John",
    years: 30
  };
  
  // Напишіть деструктуруюче присвоєння, яке:
  // властивість name присвоїть в змінну ім'я
  // властивість years присвоїть в змінну вік
  // властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті
  // Виведіть змінні на екран.
  
  const {name, years, isAdmin = false } = user1;
  


let ul = document.createElement('ul');
document.body.append(ul);

console.log(ul);
ul.innerHTML = `  
<li>name: ${name} </li>  
<li>years: ${years} </li>  
<li>Is admin: ${isAdmin} </li>  
`;

